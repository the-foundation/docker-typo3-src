# Docker Typo3 Src

typo3 container that automatically downloads the most recent typo3 source to `/var/www/typo3_src` to use e.g. volumes-from or mount this directory read-only

## Typo3shell
the `typo3shell` command drops a user into the right place for e.g deletion

you might install it by:
* making it executable and install it (as root : `cp -aurv typo3shell /usr/bin/typo3shell;chmod +x /usr/bin/typo3shell`)
if you use sudo:
* add a group `addgroup typoadmin`
* add users to this group `adduser typoadmin myusername`
* put the following line into /etc/sudoers:
   `%typoadmin ALL=(ALL) NOPASSWD: /usr/bin/typo3shell`

---

<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-typo3/README.md/logo.jpg" width="480" height="270"/></div></a>

