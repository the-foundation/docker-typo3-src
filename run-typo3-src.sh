#!/bin/bash

function filterlog() {     sed 's~/var/www/~~g' ; } ;
    

echo "##TYPO3-SRC START##" 
test -d /var/www/typo3_src      || mkdir -p /var/www/typo3_src 
test -e /var/www/typo3_src/.tmp || mkdir -p /var/www/typo3_src/.tmp 

chown 33:33 /var/www/typo3_src
echo "#EXISTING VERSIONS#"
ls -1 /var/www/typo3_src |sed 's/\.tar\.gz\.sig//g;s/\.tar\.gz//g'|sort -u|sed 's/$/|/g'| tr -d '\n' 
#while (true);do 
  test -f /var/www/typo3_src/.tmp/.temp.typo3.log && rm /var/www/typo3_src/.tmp/.temp.typo3.log
  test -e /var/www/typo3_src/.tmp/t3extracted && find /var/www/typo3_src/.tmp/t3extracted -delete
  echo -n "GPG";
  wget -qO- https://get.typo3.org/KEYS | gpg --import
  echo "GETTING VERSIONS"
  t3versions=$(curl https://get.typo3.org/|sed 's/href=/\nhref=/' |grep version|grep href|cut -d\" -f2) 
  for curt3version in ${t3versions};do  
    test -f /var/www/typo3_src/.tmp/tar.gz && rm /var/www/typo3_src/.tmp/tar.gz
    test -f /var/www/typo3_src/.tmp/tar.gz.sig && rm /var/www/typo3_src/.tmp/tar.gz.sig
    filelist=$(wget -q -O- https://get.typo3.org${curt3version}|grep get.typo3|grep tar.gz |sed 's/href=/\nhref=/' |grep href|cut -d\" -f2 );
    #echo ${filelist};
    echo -n "getting $curt3version signature >>"
    cd /var/www/typo3_src/.tmp/;
    echo -n "load sig :"
    sigurl=$(echo ${filelist}|sed 's/ /\n/g'|grep gz.sig|head -n1)
    if [ -z "$sigurl" ] ; then echo -n " not available";else wget -qc ${sigurl};fi
    echo -n "load tar :"
    tarurl=$(echo ${filelist}|sed 's/ /\n/g'|grep -v gz.sig|grep tar.gz|head -n1)
    if [ -z "$tarurl" ] ; then echo -n " not available";else wget -qc ${tarurl};fi
    echo
    #signame=$(echo ${filelist}|sed 's/ /\n/g'|grep gz.sig|head -n1|sed 's/.\+\///g')
    #keyID=$(echo gpg /var/www/typo3_src/.tmp/"${signame}" 2>&1 |grep "key ID "|sed 's/.\+key ID //g')
    #echo gpg --recv-keys $keyID
    checksumOK=no
    gpg --verify /var/www/typo3_src/.tmp/tar.gz.sig /var/www/typo3_src/.tmp/tar.gz 2>/dev/null && checksumOK=yes
    
    echo $checksumOK|grep -q ^yes$ && ( echo " >>checksum OK" ;
    mkdir /var/www/typo3_src/.tmp/t3extracted;
    chown 33:33 /var/www/typo3_src/.tmp/t3extracted
    chown 33:33 /var/www/typo3_src/.tmp/tar.gz
    echo -n untar"::"
    su -s /bin/bash -c "tar xzf /var/www/typo3_src/.tmp/tar.gz -C /var/www/typo3_src/.tmp/t3extracted" www-data
    cd /var/www/typo3_src/.tmp/t3extracted;current_typo=$(ls -1)
    
    version_exists=no
    test -d /var/www/typo3_src/$current_typo && version_exists=yes
    echo $version_exists|grep -q "^yes$" || ( echo installing $current_typo;
                                          mv -nv /var/www/typo3_src/.tmp/t3extracted/$current_typo /var/www/typo3_src/$current_typo |tr -d '\n';
                                           #chown -R 33:33 /var/www/typo3_src/$current_typo ;
                                           # chmod -R ugo-w /var/www/typo3_src/$current_typo 
                                           echo "installing sig and tar"
                                           cp  /var/www/typo3_src/.tmp/tar.gz.sig /var/www/typo3_src/$current_typo.tar.gz.sig 
                                           cp  /var/www/typo3_src/.tmp/tar.gz     /var/www/typo3_src/$current_typo.tar.gz
                                           echo)
    echo $version_exists|grep -q "^yes$" && ( echo "version exists"; 
                                          testresult=$(tar --compare  --file=/var/www/typo3_src/.tmp/tar.gz -C /var/www/typo3_src);
                                          echo -n "verification log count excluding uid/gid: "
                                          echo "$testresult" |grep  -v -e ^$ -e "Uid differs"$ -e "Gid differs" |wc -l |grep ^0$ || (echo "$testresult" |grep  -v -e ^$ -e "Uid differs"$ -e "Gid differs" |wc -l  ; echo "ERROR: FILES CHANGED ON DISK FOR $current_typo" ; echo "$testresult")

                                           echo )

                                            #su -s /bin/bash -c "tar --compare --group=33 --owner=33 --numeric-owner --file=/var/www/typo3_src/.tmp/tar.gz -C /var/www/typo3_src ||echo ERROR: FILES CHANGED ON DISK FOR $current_typo" www-data  ; echo )
    find /var/www/typo3_src/.tmp/t3extracted -delete

    )
  sleep 2
  done
find /var/www/typo3_src/.tmp/t3extracted -delete

## END installer 
test -f /var/www/typo3_src/.tmp/tar.gz && rm /var/www/typo3_src/.tmp/tar.gz
test -f /var/www/typo3_src/.tmp/tar.gz.sig && rm /var/www/typo3_src/.tmp/tar.gz.sig

## FillInTheBlank - Get missing tar.gz and sigs if possible

for typoversion in $(ls /var/www/typo3_src/typo3_src-* -1d|grep -v tar.gz|cut -d/ -f5);do 
test -f /var/www/typo3_src/.tmp/tar.gz && rm /var/www/typo3_src/.tmp/tar.gz
test -f /var/www/typo3_src/.tmp/tar.gz.sig && rm /var/www/typo3_src/.tmp/tar.gz.sig

           test -f /var/www/typo3_src/$typoversion".tar.gz.sig" ||( echo -n ;
           echo $typoversion" : no tar.gz.sig for "${typoversion//typo3_src-/}
           echo -n "getting $curt3version signature >>"
           cd /var/www/typo3_src/.tmp/;
           echo -n "load sig :"
           sigurl=https://get.typo3.org/${typoversion//typo3_src-/}/tar.gz.sig
           if [ -z "$sigurl" ] ; then echo -n " not available ";else echo -n " $sigurl ";wget -qc ${sigurl};fi
           echo -n "load tar :"
           tarurl=https://get.typo3.org/${typoversion//typo3_src-/}/tar.gz
           if [ -z "$tarurl" ] ; then echo -n " not available ";else echo -n " $tarurl ";wget -qc ${tarurl};fi
           echo
           #signame=$(echo ${filelist}|sed 's/ /\n/g'|grep gz.sig|head -n1|sed 's/.\+\///g')
           #keyID=$(echo gpg /var/www/typo3_src/.tmp/"${signame}" 2>&1 |grep "key ID "|sed 's/.\+key ID //g')
           #echo gpg --recv-keys $keyID
           checksumOK=no
           gpg --verify /var/www/typo3_src/.tmp/tar.gz.sig /var/www/typo3_src/.tmp/tar.gz 2>/dev/null && checksumOK=yes
           
           echo $checksumOK|grep -q ^yes$ && ( echo " >> checksum OK >>" ;
           chown 33:33 /var/www/typo3_src/.tmp/tar.gz
           cp  /var/www/typo3_src/.tmp/tar.gz.sig /var/www/typo3_src/$typoversion".tar.gz.sig"
           cp  /var/www/typo3_src/.tmp/tar.gz     /var/www/typo3_src/$typoversion".tar.gz"

            );
          ##END DOWNLOAD
           echo -n)
          #^^END test -f
    done
## END FOR typoversion

##END FITB ##
echo testing $current_typo $(ls -1d /var/www/typo3_src/* /var/www/typo3_src/.tmp/t3extracted/* |sed 's/\.tar\.gz\.sig//g;s/\.tar\.gz//g'|sort -u|sed 's/$/|/g'|tr -d '\n'  ) |filterlog|sed 's~typo3_src/typo3_src~typo3_~g'

### verify local versions
for veryfile in /var/www/typo3_src/*.tar.gz;do 
    checksumOK=no
    echo -n  "verifying existing file "$veryfile  | filterlog
    gpg --verify $veryfile".sig" $veryfile 2>/dev/null && checksumOK=yes
        (
        echo $checksumOK|grep -q ^yes$ &&  echo -n " >> checksum OK for $veryfile " 
        echo $checksumOK|grep -q ^yes$ ||  ( echo  "checksum ERROR for $veryfile"  )  
        )| filterlog |tee -a  /var/www/typo3_src/.tmp/.temp.typo3.log |tr -d '\n'

    echo $checksumOK|grep -q ^yes$ &&     (
    testresult=$(tar --compare  --file=$veryfile -C /var/www/typo3_src 2>&1 );
    echo -n " ::: verification fail count excluding uid/gid: "
    echo "$testresult" |grep  -v -e ^$ -e "Uid differs"$ -e "Gid differs" |wc -l |grep ^0$ || (echo "$testresult" |grep  -v -e ^$ -e "Uid differs"$ -e "Gid differs" |wc -l  ; ( echo "ERROR: FILES CHANGED ON DISK FOR $veryfile" ;echo; echo "$testresult" |grep  -v -e ^$ -e "Uid differs"$ -e "Gid differs" )|tee -a  /var/www/typo3_src/.tmp/.temp.typo3.log |wc -l ) ##testresult not zero
    ) ##ChecksumOK  
    echo 
  sleep 2; ## leave time between checksums
done
test -e /var/www/typo3_src/.tmp/tar.gz && rm /var/www/typo3_src/.tmp/tar.gz

##end verify 
echo "REPLACING LOG"
cat /var/www/typo3_src/.tmp/.temp.typo3.log > /typo3status.log

